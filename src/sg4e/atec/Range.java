/*
 * The MIT License
 *
 * Copyright 2018 sg4e.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package sg4e.atec;

import java.util.Collection;

/**
 * 
 * @param <T>
 */
public class Range<T> {
    private final int lowerBound, upperBound; //inclusive
    private final T value;

    public Range(int lowerBound, int upperBound, T value) {
        if(upperBound < lowerBound)
            throw new IllegalArgumentException("Upper bound < lower bound");
        this.lowerBound = lowerBound;
        this.upperBound = upperBound;
        this.value = value;
    }
    
    public boolean contains(int value) {
        return value >= lowerBound && value <= upperBound;
    }
    
    public T getValue() {
        return value;
    }
    
    public static <T> T getValueFromRanges(Collection<Range<T>> ranges, int point) {
        for(Range<T> range : ranges) {
            if(range.contains(point))
                return range.getValue();
        }
        return null;
    }
}
