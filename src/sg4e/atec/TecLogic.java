/*
 * The MIT License
 *
 * Copyright 2018 sg4e.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package sg4e.atec;

import java.util.ArrayList;
import java.util.List;

/**
 * 
 */
public class TecLogic {
    private final List<Range<Integer>> ranges;
    
    public TecLogic(Range<Integer> r1, Range<Integer> r2, Range<Integer> r3, 
            Range<Integer> r4, Range<Integer> r5) {
        this.ranges = new ArrayList<>();
        ranges.add(r1);
        ranges.add(r2);
        ranges.add(r3);
        ranges.add(r4);
        ranges.add(r5);
    }
    
    public int evaluate(int value) {
        Integer returnValue = Range.getValueFromRanges(ranges, value);
        if(returnValue == null)
            return TecPanel.TEC_INPUT_ERROR;
        else
            return returnValue;
    }
    
    public int getResetValue() {
        return 0;
    }
}
